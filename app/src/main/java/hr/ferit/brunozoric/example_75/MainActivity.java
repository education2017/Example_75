package hr.ferit.brunozoric.example_75;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity implements View.OnClickListener {

	Button bStartSecondActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.initialize();
	}

	private void initialize() {
		this.bStartSecondActivity = (Button) this.findViewById(R.id.bStartSecondActivity);
		this.bStartSecondActivity.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(this, SecondActivity.class);
		// This can be read from EditText
		String email = "bruno.zoric@ferit.hr";
		int age = 30;
		intent.putExtra(SecondActivity.EMAIL_EXTRA, email);
		intent.putExtra(SecondActivity.AGE_EXTRA, age);

		this.startActivity(intent);
	}
}

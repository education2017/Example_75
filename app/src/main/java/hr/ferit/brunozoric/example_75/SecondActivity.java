package hr.ferit.brunozoric.example_75;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends Activity {

	public static String AGE_EXTRA = "age_extra";
	public static String EMAIL_EXTRA = "email_extra";

	TextView tvEmailDisplay, tvAgeDisplay;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);

		this.initialize();

		Intent startingIntent = this.getIntent();
		this.handleExtraData(startingIntent);
	}

	private void handleExtraData(Intent startingIntent) {
		if(startingIntent.hasExtra(AGE_EXTRA)){
			int age = startingIntent.getIntExtra(AGE_EXTRA, 0);
			tvAgeDisplay.setText(String.valueOf(age));
		}
		if(startingIntent.hasExtra(EMAIL_EXTRA)){
			String email = startingIntent.getStringExtra(EMAIL_EXTRA);
			tvEmailDisplay.setText(email);
		}
	}

	private void initialize() {
		this.tvAgeDisplay = (TextView) this.findViewById(R.id.tvAgeDisplay);
		this.tvEmailDisplay = (TextView) this.findViewById(R.id.tvEmailDisplay);
	}
}
